PKG := bshelton229/goapp

clean:
	rm -rf ./bin/*

bin/goapp:
	GOOS=linux GOARCH=amd64 go build -o bin/goapp .

docker-build: bin/goapp
	docker build -t $(PKG) .

docker-push: docker-build
	docker push $(PKG)

docker-run: docker-build
	docker run --rm -p 8010:8010 $(PKG)
