FROM alpine

COPY ./bin/goapp /usr/bin/goapp

ENV PORT=8010
EXPOSE 8010

CMD ["/usr/bin/goapp"]
